import React from 'react';

import '@testing-library/jest-dom/extend-expect';
import '@testing-library/react/cleanup-after-each';

import { render, fireEvent } from '@testing-library/react';

import axios from 'axios';
import 'babel-polyfill';

// eslint-disable-next-line import/no-extraneous-dependencies
import { waitForElement } from '@testing-library/dom';
import OrderComponent from '../Order';

jest.mock('axios'); // Mock axios模块

test('Order组件显示异步调用订单数据', async () => {
  // <--start
  // TODO 4: 给出正确的测试
  // setup组件
  const { getByTestId, getByLabelText } = render(<OrderComponent />);
  // Mock数据请求
  const resp = { data: { status: '已完成' } };
  axios.get.mockResolvedValue(resp);
  // 触发事件
  await waitForElement(() => fireEvent.click(getByLabelText('submit-button'), { button: 0 }), {
    container: getByTestId('status')
  });
  // 给出断言
  expect(getByTestId('status')).toHaveTextContent('已完成');
  // --end->
});
